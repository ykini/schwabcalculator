# SchwabCalculator

Parse output of Charles Schwab CSV/XLS file and Compute tax liability for stock sales

Usage:
* Log into your equity award account
* Got to history and choose all transactions within a custom range. This is typically one financial year
i.e say 1st April 2019 till 31 March 2020.
*Top right corner has a export button. Hit that and this saves a CSV file
* Next you need to convert this as is to an xlsx file using MS excel.
* Edit the file main.py to choose input and output excel workbooks.
* Run the script main.py to generate the output workbook.
* Validate the output workbook. The columns are self explanatory.

Requirements:
* You would need to install python, openpyxl, forex_python
* pip install openpyxl
* pip install forex_python

Known Issues:
* Does not compute the dividend taxes
* More input validations planned, but not implemented
* Forex exchange values seem to differ slightly from google finance. Not sure how reliable they are

  