

from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.styles import NamedStyle, Font, Border, Side
from openpyxl.utils import get_column_letter

from forex_python.converter import CurrencyRates

import datetime
from datetime import date

import tkinter as tk
from tkinter import filedialog,scrolledtext, ttk

import requests
import datetime
import os
import threading

###########################
# Classes for Sale Record
###########################
class StockSale:
    typeOfStock = ""
    numberSold = 0
    saleDate = None       # list with day month year
    purchaseDate = None   # list with day month year
    purchaseDateNative = datetime.MINYEAR
    saleDateNative = datetime.MINYEAR
    purchaseFMV = 0.0
    purchaseValueINR = 0.0
    purchaseValueWithIndex = 0.0
    purchaseConvRate = 0.0
    saleValueINR = 0.0
    saleValue = 0.0
    saleConvRate = 0.0
    longTerm = False
    profit = 0.0
    taxForTransaction = 0.0

listOfSales = []
TotalTax = 0.0
verbose = 0


def get_exchange_rate(date):
    """Returns the exchange rate of USD to INR on the given date"""
    date = date.strftime("%Y-%m-%d")
    api_url = "https://openexchangerates.org/api/historical/{}.json".format(date)

    params = {
        "app_id": "afeaf530d4564f76a3bc4a5061bc3fdd",  # Replace with your API key
        "symbols": "INR",
        "base": "USD"
    }
    response = requests.get(api_url, params=params)
    if response.status_code == 200:
        exchange_rates = response.json()["rates"]
        exchange_rate = exchange_rates["INR"]
        return exchange_rate
    else:
        raise Exception("Failed to retrieve exchange rate for {}".format(date))



###########################
# Compute Tax
###########################
def get_index_value_for_year(tDate):
    if(date(2023, 4, 1) <= tDate.date() <= date(2024, 3, 31)):
        return 348
    if(date(2022, 4, 1) <= tDate.date() <= date(2023, 3, 31)):
        return 331
    if(date(2021, 4, 1) <= tDate.date() <= date(2022, 3, 31)):
        return 317
    elif(date(2020, 4, 1) <= tDate.date() <= date(2021, 3, 31)):
        return 301
    elif(date(2019, 4, 1) <= tDate.date() <= date(2020, 3, 31)):
        return 289
    elif (date(2018, 4, 1) <= tDate.date() <= date(2019, 3, 31)):
        return 280
    elif (date(2017, 4, 1) <= tDate.date() <= date(2018, 3, 31)):
        return 272
    elif (date(2016, 4, 1) <= tDate.date() <= date(2017, 3, 31)):
        return 264
    elif (date(2015, 4, 1) <= tDate.date() <= date(2016, 3, 31)):
        return 254
    elif (date(2014, 4, 1) <= tDate.date() <= date(2015, 3, 31)):
        return 240
    elif (date(2013, 4, 1) <= tDate.date() <= date(2014, 3, 31)):
        return 220
    elif (date(2012, 4, 1) <= tDate.date() <= date(2013, 3, 31)):
        return 200
    elif (date(2011, 4, 1) <= tDate.date() <= date(2012, 3, 31)):
        return 184
    elif (date(2010, 4, 1) <= tDate.date() <= date(2011, 3, 31)):
        return 167
    elif (date(2009, 4, 1) <= tDate.date() <= date(2010, 3, 31)):
        return 148
    elif (date(2008, 4, 1) <= tDate.date() <= date(2009, 3, 31)):
        return 137

def get_index_value(purchseDate, saleDate):
    return float(get_index_value_for_year(saleDate))/float(get_index_value_for_year(purchseDate))



def compute_tax():
    print('Total Sale Records : ' + str(len(listOfSales)))
    global TotalTax
    c = CurrencyRates()
    tCount = 0
    for rec in listOfSales:
        # Compute salePrice in INR
        convFactor = get_exchange_rate(rec.saleDateNative)
        rec.saleConvRate = convFactor
        rec.saleValueINR = convFactor * rec.saleValue * rec.numberSold
        #print("DEBUG " + str(convFactor) + " " + str(rec.saleValueINR))

        # Compute Purchase Value
        convFactor = get_exchange_rate(rec.purchaseDateNative)
        rec.purchseConvRate = convFactor
        rec.purchaseValueINR = convFactor * rec.purchaseFMV * rec.numberSold

        #print("DEBUG Purchase Value" + " " + str(rec.purchaseDateNative) + " " + str(convFactor) + " " + str(rec.purchaseValueINR) )

        # Long Term or Short Term
        delta = rec.saleDateNative - rec.purchaseDateNative
        #print("DEBUG Delta " + str(delta.days))

        if (delta.days > 365 * 2):
            rec.longTerm = True
        else:
            rec.longTerm = False

        #print("DEBUG Long Term: " + str(rec.longTerm))

        if (rec.longTerm == True):
            # Compute LongTerm Tax
            indexation = get_index_value(rec.purchaseDateNative, rec.saleDateNative)
            rec.purchaseValueWithIndex = rec.purchaseValueINR * indexation

            # Long term tax is 20.8 percent
            # Negative tax if loss is made
            rec.profit = rec.saleValueINR - rec.purchaseValueWithIndex
            rec.taxForTransaction = (rec.saleValueINR - rec.purchaseValueWithIndex) * 0.208
        else:
            # Compute ShortTerm Tax
            # Short Term Tax is 30%
            rec.taxForTransaction = (rec.saleValueINR - rec.purchaseValueINR) * 0.3
            rec.profit = rec.saleValueINR - rec.purchaseValueINR

        if (verbose == 1):
            print("SALE Begin Transaction: ================" )
            print("Stocks sold: " + str(rec.numberSold) + " @ " + str(rec.saleValue))
            print("Buy Price INR: " + str(rec.purchaseValueINR))
            print("Buy Price with Index: " + str(rec.purchaseValueWithIndex))
            print("Sale Price : " + str(rec.saleValueINR))
            print("Profit : " + str(rec.profit))
            print("SALE End Transaction: ================")

        TotalTax = TotalTax + rec.taxForTransaction
        tCount = tCount + 1
        file_picker_dialog.mark_progress(tCount/len(listOfSales)*100)
    print("Total Tax is : ", TotalTax)


###########################
# Parse Functions
###########################

def parse_espp_row(ws, rowNumber, saleDate):
    if (verbose == 1):
        print('Process ESPP Row')
        print('     Count = ', str(ws['C'][rowNumber].value))
        print('     Sale Value = ', str(ws['D'][rowNumber].value))
        print('     Sale Date = ', saleDate)
        print('     Purchase Date = ', str(ws['G'][rowNumber].value))
        print('     Purchase FMV = ', str(ws['I'][rowNumber].value))

    rec = StockSale()
    rec.TypeOfStock = "ESPP"
    rec.numberSold  = int(ws['C'][rowNumber].value)
    rec.saleValue   = float(ws['D'][rowNumber].value)

    # Dates in XLS are in the format 2020-05-28 00:00:00
    tDate = str(saleDate).rsplit(' ')
    rec.saleDate = tDate[0].rsplit('-')

    pDate = str(ws['G'][rowNumber].value);
    tDate = pDate.rsplit(' ')
    rec.purchaseDate = tDate[0].rsplit('-')

    rec.purchaseFMV = float(ws['I'][rowNumber].value)

    rec.purchaseDateNative = ws['G'][rowNumber].value
    rec.saleDateNative = saleDate

    listOfSales.append(rec)

def parse_rsu_row(ws, rowNumber, saleDate):
    if (verbose == 1):
        print('Process RSU Row:')
        print('     Count = ', str(ws['C'][rowNumber].value))
        print('     Sale Value = ', str(ws['D'][rowNumber].value))
        print('     Sale Date = ', saleDate)
        print('     Vest Date = ', str(ws['L'][rowNumber].value))
        print('     Vest FMV = ', str(ws['M'][rowNumber].value))

    rec = StockSale()
    rec.TypeOfStock = "RSU"
    rec.numberSold = int(ws['C'][rowNumber].value)
    rec.saleValue = float(ws['D'][rowNumber].value)

    # Dates in XLS are in the format 2020-05-28 00:00:00
    tDate = str(saleDate).rsplit(' ')
    rec.saleDate = tDate[0].rsplit('-')


    pDate = str(ws['L'][rowNumber].value);
    tDate = pDate.rsplit(' ')
    rec.purchaseDate = tDate[0].rsplit('-')

    rec.purchaseFMV = float(ws['M'][rowNumber].value)

    rec.purchaseDateNative = ws['L'][rowNumber].value
    rec.saleDateNative = saleDate

    listOfSales.append(rec)

def parse_sale_record(ws, rowNumber):
    #print('Processing Sale at Row ' + str(rowNumber))
    file_picker_dialog.log('Processing Sale at Row ' + str(rowNumber)+'\n')
    saleDate = ws['A'][rowNumber-1].value
    if (ws['B'][rowNumber].value == 'Type'):
        r = rowNumber + 1
        while True:
            if (r  >= len(ws['B'])):
                break
            elif (ws['B'][r].value == 'ESPP'):
                parse_espp_row(ws, r, saleDate)
                r = r + 1
            elif (ws['B'][r].value == 'RS'):
                parse_rsu_row(ws, r, saleDate)
                r = r + 1
            elif (ws['B'][r].value == 'Type'): # Skip "Type" rows
                r = r + 1
            else:
                break
    else:
        print("Broken XLS sheet")

######################################
# Save output file to XLS
######################################

def save_output_to_excel(filename):
    wb_output = Workbook()
    wso = wb_output.active
    wso.title = 'TaxComputation'

    # Need to create Cells in advance
    for x in range(1, (len(listOfSales) + 10)):
        for y in range(1, 25):
            wso.cell(row=x, column=y)

    count = 2

    wso['B'][count].value = "Type"
    wso['C'][count].value = "Number of Stocks"
    wso['D'][count].value = "Purchase/Vest FMV"
    wso['E'][count].value = "Purchase Date"
    wso['F'][count].value = "Purchase Value INR"
    wso['G'][count].value = "Purchase Value with Index"
    wso['H'][count].value = "Purchase Conversion Rate"
    wso['I'][count].value = "Sale Date"
    wso['J'][count].value = "Sale Value"
    wso['K'][count].value = "Sale Value INR"
    wso['L'][count].value = "Sale Conversion Rate"
    wso['M'][count].value = "Profit"
    wso['N'][count].value = "Long Term"
    wso['O'][count].value = "Tax"

    count = count + 1

    for rec in listOfSales:
        wso['B'][count].value = rec.TypeOfStock
        wso['C'][count].value = rec.numberSold

        wso['D'][count].value = rec.purchaseFMV
        wso['D'][count].style = 'Currency'

        wso['E'][count].value = str(rec.purchaseDateNative.date())

        wso['F'][count].value = rec.purchaseValueINR
        wso['F'][count].style = 'Comma'

        wso['G'][count].value = rec.purchaseValueWithIndex
        wso['G'][count].style = 'Comma'

        wso['H'][count].value = rec.purchseConvRate
        wso['I'][count].value = str(rec.saleDateNative.date())

        wso['J'][count].value = rec.saleValue
        wso['J'][count].style = 'Currency'

        wso['K'][count].value = rec.saleValueINR
        wso['K'][count].style = 'Comma'

        wso['L'][count].value = rec.saleConvRate

        wso['M'][count].value = rec.profit
        wso['M'][count].style = 'Comma'

        wso['N'][count].value = rec.longTerm

        wso['O'][count].value = rec.taxForTransaction
        wso['O'][count].style = 'Comma'

        count = count + 1

    wso['B'][count + 2].value = 'Total Tax :'
    wso['C'][count+2].value = TotalTax
    wso['C'][count + 2].style = 'Total'

    for cell in wso[3]:
        cell.style = 'Headline 3'

    # Adjust Column widths
    dims = {}
    for row in wso.rows:
        for cell in row:
            if cell.value:
                dims[cell.column_letter] = max((dims.get(cell.column_letter, 0), len(str(cell.value))))
    for col, value in dims.items():
        wso.column_dimensions[col].width = value * 1.2
    # Adjust Column widths

    wb_output.save(filename)





filepath = '/mnt/c/Users/ykini/Documents/SchwabData/Input-2023.xlsx'
outputFile = '/mnt/c/Users/ykini/Documents/SchwabData/Output-2023.xlsx'

def do_calculate_stuff(input_file):
    wb = load_workbook(input_file)
    ws = wb.active

    colB = ws['B']
    count = 1

    for cell in colB:
        if cell.value == 'Sale':
            parse_sale_record(ws, count)
        count = count + 1
    
    compute_tax()
    input_dir = os.path.dirname(input_file)
    input_base = os.path.basename(input_file)
    output_base, output_ext = os.path.splitext(input_base)
    output_base = f"{output_base}-output"
    output_file = os.path.join(input_dir, f"{output_base}{output_ext}")

    save_output_to_excel(output_file) 
    file_picker_dialog.log('Completed Processing\n')
    file_picker_dialog.log('Saved to file : ' + output_file + '\n')

class FilePickerDialog:
    def __init__(self, master):
        self.master = master
        self.file_path = None

        # Set dialog size and center it
        self.master.geometry('360x340')
        self.master.eval('tk::PlaceWindow . center')
        self.master.resizable(False, False)

        # Create a label to display the selected file path
        self.file_path_label = tk.Label(self.master, text='No file selected')
        self.file_path_label.grid(row=0, column=1, padx=10, pady=10, sticky='W')
        

        # Create a file picker button
        self.file_picker_button = tk.Button(self.master, text='Select File', command=self.pick_file)
        self.file_picker_button.grid(row=0, column=0, padx=10, pady=10, sticky='W') 
        

        # Create a calculate button (disabled by default)
        self.calculate_button = tk.Button(self.master, text='Calculate', command=self.do_stuff, state='disabled')
        self.calculate_button.grid(row=1, column=0, padx=10, pady=10, sticky='W')
        

        # Create a console box to display logs
        self.console = scrolledtext.ScrolledText(self.master, width=40, height=10, state='disabled')
        self.console.grid(row=3, column=0,columnspan=4, padx=10, pady=10, sticky='W')
       

        # Progress bar
        self.progress_bar = ttk.Progressbar(self.master, orient=tk.HORIZONTAL, length=320, mode='determinate')
        self.progress_bar.grid(row=2, column=0,columnspan=4, padx=10, pady=10, sticky='W')
        

    def pick_file(self):
        # Show file picker dialog and set allowed file types
        file_path = filedialog.askopenfilename(
            title='Select Excel file',
            filetypes=[('Excel files', '*.xls;*.xlsx')]
        )

        if file_path:
            self.file_path = file_path
            self.file_path_label.config(text=file_path)
            self.calculate_button.config(state='normal')
        else:
            self.file_path = None
            self.file_path_label.config(text='No file selected')
            self.calculate_button.config(state='disabled')

    def log(self, text):
        self.console.configure(state='normal')
        self.console.insert(tk.END, text)
        self.console.see(tk.END)
        self.console.configure(state='disabled')
        self.console.update()
    
    def mark_progress(self, percent_complete):
        self.progress_bar['value'] = percent_complete
        self.progress_bar.update()

    def do_stuff(self):
        # Replace this with your actual calculation function
        self.console.insert('end', f'Calculating with file: {self.file_path}\n')
        global listOfSales 
        global TotalTax
        
        listOfSales = []
        TotalTax = 0.0
        
        #do_calculate_stuff(self.file_path)
        thread = threading.Thread(target=do_calculate_stuff, args=(self.file_path,))
        thread.start()
        self.console.see('end')
        

if __name__ == '__main__':
    root = tk.Tk()
    file_picker_dialog = FilePickerDialog(root)
    
    root.mainloop()






